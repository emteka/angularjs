var myApp = angular
    .module("app", ["ngRoute", "orderController"])
    .config(function($routeProvider) {
        $routeProvider
        .when("/order", {
            templateUrl: "views/order.html"
        })
        .when("/summary", {
            templateUrl: "views/summary.html"
        })
        .otherwise({
        templateUrl: "views/order.html"
        })
    })
