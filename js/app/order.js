angular
.module("orderController", [])
.controller('orderController', ['$scope', function($scope){
            $scope.orders = [
                {'color': 'asd', 'size': 'S'}
            ];
            function countObjs(hashs){
                  var count = 0
                  for(var i = 0; i < $scope.orders.length; i++){
                    if($scope.orders[i].hash === hashs)
                        count += 1;
                  }
                  return count;
              }
            $scope.placeOrder = function(){
            var hash = $scope.size.concat($scope.color)
                $scope.orders.push({
                'color': $scope.color,
                'size': $scope.size,
                'hash': hash
                })
                var div = document.getElementById(hash);
                if ('null' != div) {
                var text = countObjs(hash);
            div.innerText = text;
}}
        }]
    ).filter('unique', function () {

           return function (items, filterOn) {

             if (filterOn === false) {
               return items;
             }

             if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
               var hashCheck = {}, newItems = [];

               var extractValueToCompare = function (item) {
                 if (angular.isObject(item) && angular.isString(filterOn)) {
                   return item[filterOn];
                 } else {
                   return item;
                 }
               };

               angular.forEach(items, function (item) {
                 var valueToCheck, isDuplicate = false;

                 for (var i = 0; i < newItems.length; i++) {
                   if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                     isDuplicate = true;
                     break;
                   }
                 }
                 if (!isDuplicate) {
                   newItems.push(item);
                 }

               });
               items = newItems;
             }
             return items;
           };
         });